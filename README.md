## Why should we use Json Web Token(JWT)?

*  **Authorization:** This is the most common scenario for using JWT. Once the user is logged in,
each subsequent request will include the JWT, allowing the user to access routes, services,
and resources that are permitted with that token. Single Sign On is a feature that widely uses JWT nowadays,
because of its small overhead and its ability to be easily used across different domains


*  **Information Exchange** JSON Web Tokens are a good way of securely transmitting information between parties.
Because JWTs can be signed—for example, using public/private key pairs—you can be sure the senders are who 
they say they are.Additionally, as the signature is calculated using the header and the payload,
you can also verify that the content hasn't been tampered with.

## Implementation
``` javascript
 router.post('/login', (req, res, next) => {
        let fetchedUser;
        User.findOne({username:req.body.username })
            .then(user => { 
                if (!user) {  
                    return res.status(401).json(
                        {
                            message: 'Auth failed'
                        });
                }

                fetchedUser=user;
                console.log(req.body.password);
                return bcrypt.compare(req.body.password, user.password);
            })
            .then(result => {
                if (!result){     
                    return res.status(401).json(
                        {
                            message: 'Auth failed'
                        })
                     }
                     
        //creating a token if the username and password matched
                const token = jwt.sign(
                    { username: fetchedUser.username}, 
                    'this_is_a_complete_long_secret_pass',
                    { expiresIn: '1h' })
                  res.status(200).json(//responding token as  a json
                    {
                        token: token
                    })
            })
            .catch(err => {
                console.log(err);
                return res.status(401).json(
                    {
                        message: 'Auth failed'
                    })
            })
    });
  ```
### At Client Side

``` javascript
loginUser(username:string,password:string){
    const user:Auth={username:username,password:password}
    this.http.post<{token:string}>('http://localhost:3000/users/login',user)
    .subscribe(response=>{
       this.token=response.token; //getting a token as response
       }
    })
}
```
